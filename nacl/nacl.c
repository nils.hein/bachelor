#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "my_numerics.h"
#include "seed.c"

/* Vektor Struktur */
typedef struct {
    double x;
    double y;
} Vector;

/*------------------------  PHYSIKALISCHE KONSTANTEN  -------------------------*/

/*
const int N = 8;              // Anzahl der Teilchen
const double r_0 = 0.321e-10;           //[m]
const double mass_cl = 35.45*1.66e-27;        // Masse von Chlor [kg]
const double mass_na = 22.98*1.66e-27;       //Masse von Natrium [kg]
const double elem = 1.609e-19;  //Elementarladung
const double eps_0 = 8.854e-12; //epsilon 0
const double V_0 = 1.09e3*1.609e-19;           //Nullpotential
const double delta_x = 1e-3;           //Abstandsschritt zum Feinden des Ausgangszustandes
const double Temp = 300/3400;        //Temperature [K]
const double k_b = 1.3806e-23; //Boltzmann constant
*/

const int N = 3;              // Anzahl der Teilchen
const double r_0 = 0.321e-10;           //[m]
const double mass_cl = 35.45*1.66e-27;        // Masse von Chlor [kg]
const double mass_na = 22.98*1.66e-27;       //Masse von Natrium [kg]
const double elem = 1.609e-19;  //Elementarladung
const double eps_0 = 8.854e-12; //epsilon 0
const double V_0 = 1.09e3*1.609e-19;           //Nullpotential
const double delta_x = 1e-3;           //Abstandsschritt zum Feinden des Ausgangszustandes
const double Temp = 300.0/3400.0;        //reduced Temperature [K]
const double k_b = 1.3806e-23; //Boltzmann constant

/*
const int N = 8;              // Anzahl der Teilchen
const double r_0 = 1;           //[m]
const double mass_cl = 1;        // Masse von Chlor [kg]
const double mass_na = 1;       //Masse von Natrium [kg]
const double elem = 1;  //Elementarladung
const double eps_0 = 1; //epsilon 0
const double V_0 = 1;           //Nullpotential
const double delta_x = 1e-8;           //Abstandsschritt zum Feinden des Ausgangszustandes
const double Temp = 300/3400;        //Temperature [K]
const double k_b = 1; //Boltzmann constant
*/

/*-------------------------  SIMULATIONS-PARAMETER  ---------------------------*/

const double T_max = 20.63;
const double dt[] = {1e-2, 1e-3, 1e-4, 1e-5}; // Zeitliche Schrittweiten

static const char pos_file_name[] = "position.dat";
static const char energy_file_name[5][12] = {"energy0.dat", "energy1.dat", "energy2.dat", "energy3.dat"};

/*-------------------------  PHYSIKALISCHES SYSTEM  ---------------------------*/
int pendumlumsODE2D(double t, const double y[], double f[], void *params)
{
    for(int i=0; i<2*N; i++){
        f[i]=0;
    }

    //Geschwindigkeiten in f von y eintragen
    for(int i=0; i<N; i++){
        f[i] = y[N+i];
    }

    double c1 = elem*elem/(4*M_PI*eps_0);

    Vector q0 = {y[0], y[1]};
    Vector q1 = {y[2], y[3]};
    Vector q2 = {y[4], y[5]};
    Vector q3 = {y[6], y[7]};

    double r10 = sqrt((q1.x-q0.x)*(q1.x-q0.x)+(q1.y-q0.y)*(q1.y-q0.y));
    double r20 = sqrt((q2.x-q0.x)*(q2.x-q0.x)+(q2.y-q0.y)*(q2.y-q0.y));
    double r30 = sqrt((q3.x-q0.x)*(q3.x-q0.x)+(q3.y-q0.y)*(q3.y-q0.y));
    double r21 = sqrt((q2.x-q1.x)*(q2.x-q1.x)+(q2.y-q1.y)*(q2.y-q1.y));
    double r31 = sqrt((q3.x-q1.x)*(q3.x-q1.x)+(q3.y-q1.y)*(q3.y-q1.y));
    double r32 = sqrt((q3.x-q2.x)*(q3.x-q2.x)+(q3.y-q2.y)*(q3.y-q2.y));

    //x und y Komponente der Geschwindigkeit des ersten Teilchens
    f[N] = -(-c1*(q1.x-q0.x)/pow(r10,3)+V_0*(q1.x-q0.x)/(r_0*r10)*exp(-r10/r_0)
        -c1*(q2.x-q0.x)/pow(r20,3)+V_0*(q2.x-q0.x)/(r_0*r20)*exp(-r20/r_0)
        +c1*(q3.x-q0.x)/pow(r30,3))/mass_na;
    f[N+1] = -(-c1*(q1.y-q0.y)/pow(r10,3)+V_0*(q1.y-q0.y)/(r_0*r10)*exp(-r10/r_0)
        -c1*(q2.y-q0.y)/pow(r20,3)+V_0*(q2.y-q0.y)/(r_0*r20)*exp(-r20/r_0)
        +c1*(q3.y-q0.y)/pow(r30,3))/mass_na;

    //x und y Komponente der Geschwindigkeit des zweiten Teilchens
    f[N+2] = -(c1*(q1.x-q0.x)/pow(r10,3)-V_0*(q1.x-q0.x)/(r_0*r10)*exp(-r10/r_0)
        +c1*(q2.x-q1.x)/pow(r21,3)
        -c1*(q3.x-q1.x)/pow(r31,3)+V_0*(q3.x-q1.x)/(r_0*r31)*exp(-r31/r_0))/mass_cl;
    f[N+3] = -(c1*(q1.y-q0.y)/pow(r10,3)-V_0*(q1.y-q0.y)/(r_0*r10)*exp(-r10/r_0)
        +c1*(q2.y-q1.y)/pow(r21,3)
        -c1*(q3.y-q1.y)/pow(r31,3)+V_0*(q3.y-q1.y)/(r_0*r31)*exp(-r31/r_0))/mass_cl;

    //x und y Komponente der Geschwindigkeit des dritten Teilchens
    f[N+4] = -(c1*(q2.x-q0.x)/pow(r20,3)-V_0*(q2.x-q0.x)/(r_0*r20)*exp(-r20/r_0)
        -c1*(q2.x-q1.x)/pow(r21,3)
        -c1*(q3.x-q2.x)/pow(r32,3)+V_0*(q3.x-q2.x)/(r_0*r32)*exp(-r32/r_0))/mass_cl;
    f[N+5] = -(c1*(q2.y-q0.y)/pow(r20,3)-V_0*(q2.y-q0.y)/(r_0*r10)*exp(-r20/r_0)
        -c1*(q2.y-q1.y)/pow(r21,3)
        -c1*(q3.y-q2.y)/pow(r32,3)+V_0*(q3.y-q2.y)/(r_0*r32)*exp(-r32/r_0))/mass_cl;

    //x und y Komponente der Geschwindigkeit des vierten Teilchens
    f[N+6] = -(c1*(q3.x-q2.x)/pow(r32,3)-V_0*(q3.x-q2.x)/(r_0*r32)*exp(-r32/r_0)
        -c1*(q3.x-q0.x)/pow(r30,3)
        +c1*(q3.x-q1.x)/pow(r31,3)-V_0*(q3.x-q1.x)/(r_0*r31)*exp(-r31/r_0))/mass_na;
    f[N+7] = -(c1*(q3.y-q2.y)/pow(r32,3)-V_0*(q3.y-q2.y)/(r_0*r32)*exp(-r32/r_0)
        -c1*(q3.y-q0.y)/pow(r30,3)
        +c1*(q3.y-q1.y)/pow(r31,3)-V_0*(q3.y-q1.y)/(r_0*r31)*exp(-r31/r_0))/mass_na;
    
    return 1;
}

int pendumlumsODE(double t, const double y[], double f[], void *params)
{
    /* Loesung fuer Aufgabe 12.4 kommt hier dazwischen: */
    for(int i=0; i<2*N; i++){
        f[i]=0;
    }

    for(int i=0; i<N; i++){
        f[i] += y[N+i];
    }

    double c1 = elem*elem/(4*M_PI*eps_0);
    double r10 = y[1]-y[0];
    double r20 = y[2]-y[0];
    double r21 = y[2]-y[1];
    

    /*
    f[N] = (elem*elem/(4*M_PI*eps_0)*((y[0]+y[1])/(pow((y[0]+y[1]),3))-(y[2]-y[0])/pow(fabs(y[2]-y[0]),3))+V_0/r_0*((y[2]-y[0])/fabs(y[2]-y[0])*exp(-fabs(y[2]-y[0])/r_0)-(y[0]+y[1])/fabs(y[0]+y[1])*exp(-fabs(y[0]+y[1])/r_0)))/mass_cl;

    f[N+1] = (elem*elem/(4*M_PI*eps_0)*((y[0]+y[1])/pow(fabs(y[0]+y[1]),3)+(y[2]-y[1])/pow(fabs(y[2]-y[1]),3))-V_0/r_0*(y[0]+y[1])/fabs(y[0]+y[1])*exp(-fabs(y[0]+y[1])/r_0))/mass_na;

    f[N+2] = (elem*elem/(4*M_PI*eps_0)*((y[1]-y[2])/pow(fabs(y[1]-y[2]),3)-(y[0]-y[2])/pow(fabs(y[0]-y[2]),3))+V_0/r_0*(y[0]-y[2])/fabs(y[0]-y[2])*exp(-fabs(y[0]-y[2])/r_0))/mass_na;
    */

    
    f[N] = (c1*r10/pow(fabs(r10),3)-V_0*r10*exp(-fabs(r10)/r_0)/(r_0*fabs(r10))-c1*r20/pow(fabs(r20),3))/mass_na;

    f[N+1] = (-c1*r10/pow(fabs(r10),3)+V_0*r10*exp(-fabs(r10)/r_0)/(r_0*fabs(r10))+c1*r21/pow(fabs(r21),3)-V_0*r21*exp(-fabs(r21)/r_0)/(r_0*fabs(r21)))/mass_cl;

    f[N+2] = (c1*r20/pow(fabs(r20),3)-c1*r21/pow(fabs(r21),3)+V_0*r21*exp(-fabs(r21)/r_0)/(r_0*fabs(r21)))/mass_na;
    

    /*
    f[N] = (c1*(1.0/(r10*r10)-1.0/(r20*r20))-V_0/r_0*exp(-r10/r_0))/mass_na;

    f[N+1] = (c1*(1.0/(r21*r21)-1.0/(r10*r10))+V_0/r_0*(exp(-r10/r_0)-exp(-r21/r_0)))/mass_cl;

    f[N+2] = (c1*(1.0/(r20*r20)-1.0/(r21*r21))+V_0/r_0*exp(-r21/r_0))/mass_na;
    */
    /* -------------------------------------------------*/
    return 1;
}

/* TODO */
/* Bearbeiten Sie hier Aufgabe 12.9 */
double pendulums_energy(const double y[]){
    double Energy = 0.0;
    double V = 1.09e3;
    /*
    double verlocity = 0.0;
    double x = y[0]*y[0];
    for(int i = 0; i<N-1; i++){
        double dx = y[i+1]-y[i]-base_length;
        x += dx*dx;
        verlocity += y[N+i]*y[N+i];
    }
    verlocity += y[2*N-1]*y[2*N-1];
    //printf("v: %15e\n", verlocity);
    //printf("x: %15e\n", x);
    double Energy = (k*x+mass*verlocity)/2;
    //printf("E: %15e\n", Energy);
    */
    double c1 = elem/(4*M_PI*eps_0);
    double r10 = y[1]-y[0];
    double r20 = y[2]-y[0];
    double r21 = y[2]-y[1];

    Energy += -c1/r10+V*exp(-fabs(r10)/r_0)+c1/fabs(r20)-c1/fabs(r21)+V*exp(-fabs(r21)/r_0);
    Energy += ((y[3]*y[3]*mass_na)/2+(y[4]*y[4]*mass_cl)/2+(y[5]*y[5]*mass_na)/2)/elem;

    return Energy;
}

double pendulums_energy2D(const double y[]){
    double Energy = 0.0;
    double c1 = elem*elem/(4*M_PI*eps_0);

    Vector q0 = {y[0], y[1]};
    Vector q1 = {y[2], y[3]};
    Vector q2 = {y[4], y[5]};
    Vector q3 = {y[6], y[7]};

    double r10 = sqrt((q1.x-q0.x)*(q1.x-q0.x)+(q1.y-q0.y)*(q1.y-q0.y));
    double r20 = sqrt((q2.x-q0.x)*(q2.x-q0.x)+(q2.y-q0.y)*(q2.y-q0.y));
    double r30 = sqrt((q3.x-q0.x)*(q3.x-q0.x)+(q3.y-q0.y)*(q3.y-q0.y));
    double r21 = sqrt((q2.x-q1.x)*(q2.x-q1.x)+(q2.y-q1.y)*(q2.y-q1.y));
    double r31 = sqrt((q3.x-q1.x)*(q3.x-q1.x)+(q3.y-q1.y)*(q3.y-q1.y));
    double r32 = sqrt((q3.x-q2.x)*(q3.x-q2.x)+(q3.y-q2.y)*(q3.y-q2.y));

    
    double r[] = {r10, r20, r32, r31, r30, r21};
    for(int i=0; i<6; i++){
        if(i<4)
            Energy += -c1/r[i]*exp(-r[i]/r_0);
        else
            Energy += c1/r[i];
    }
    
    
    for(int i=0; i<N; i+=2){
        if(i%6==0){
            Energy += (y[N+i]*y[N+i]+y[N+i+1]*y[N+i+1])*mass_na/2;
        }
        else{
            Energy += (y[N+i]*y[N+i]+y[N+i+1]*y[N+i+1])*mass_cl/2;
        }
    }

    //in eV umrechnen
    Energy = Energy/elem;

    return Energy;
}

double pendulums_kin_energy2D(const double y[]){
    double Energy = 0.0;
    
    for(int i=0; i<N; i+=2){
        if(i%6==0){
            Energy += (y[N+i]*y[N+i]+y[N+i+1]*y[N+i+1])*mass_na/2;
        }
        else{
            Energy += (y[N+i]*y[N+i]+y[N+i+1]*y[N+i+1])*mass_cl/2;
        }
    }

    //in eV umrechnen
    Energy = Energy/elem;

    return Energy;
}

void euler_step(double t, double delta_t, double y[], ode_func func, int dimension, void *params){
    double *f = malloc(dimension*sizeof(double));
    func(t, y, f, params);
    for(int i = 0; i<dimension; i++){
        //f[i] = mid_diff(y[i], delta_t, func, NULL);
        y[i]+= f[i]*delta_t;
    }
    free(f);
    return;
}

/*
void rk4_step(double t, double delta_t, double y[], ode_func func, int dimension, void *params){
    double *f = malloc(dimension*sizeof(double));
    double *y_temp = malloc(dimension*sizeof(double));
    func(t, y, f, params);
    for(int i = 0; i<dimension; i++){
        double k1 = delta_t*f[i];
        for(int j = 0; j<dimension; j++)
            y_temp[j] = y[j] + k1/2;
        func(t+delta_t*1/2, y_temp, f, params);
        double k2 = delta_t*f[i];
        for(int j = 0; j<dimension; j++)
            y_temp[j] = y[j] + k2/2;
        func(t+delta_t*1/2, y_temp, f, params);
        double k3 = delta_t*f[i];
        for(int j = 0; j<dimension; j++)
            y_temp[j] = y[j] + k3;
        func(t+delta_t, y_temp, f, params);
        double k4 = delta_t*f[i];
        y[i]+=(k1+2*k2+2*k3+k4)/6;
    }
    free(f);
    free(y_temp);
    return;
}
*/  


void verlet_step(double t, double delta_t, double y[], ode_func func, int dimension, void* params){
    double *a1 = malloc(dimension*sizeof(double));
    double *a2 = malloc(dimension*sizeof(double));

    func(t, y, a1, params);
    for(int i=0; i<dimension/2; i++){
        y[i] = y[i] + a1[i]*delta_t + a1[N+i]*delta_t*delta_t/2;
    }
    func(t+delta_t, y, a2, params);
    for(int i=0; i<dimension/2; i++){
        y[N+i] = y[N+i] + delta_t/2*(a2[N+i]+a1[N+i]);
    }

    free(a1);
    free(a2);
}
/*-----------------------------------------------------------------------------*/

double find_equi(const double dx){
    /* Dimensionalitaet des DGL-Systems */
    int gsl_dimension = 2*N;

    /* ------- Anfangswerte ---------------------------------------------------*/

    double y[gsl_dimension];
    double t = 0;
    int i = 2;
    double start = 1;
    double T_loop = T_max;
    double energy_int = 0;
    
    /* Fuellen von Array y[] mit den Startwerten fuer jeden Punkt */
    y[0] = 0;
    y[1] = dx;

    y[2] = dx;
    y[3] = dx;

    y[4] = 0;
    y[5] = 0;

    y[6] = dx;
    y[7] = 0;

    for(int j = N; j<2*N; j++)
        y[j] = 0;

    verlet_step(t, dt[3], y, pendumlumsODE2D, gsl_dimension, NULL);
    //printf("kin energy Integral: %15e\n", energy_int);
    energy_int += fabs(pendulums_kin_energy2D(y));
    //double kin_energy = pendulums_kin_energy2D(y);

    do{
        if(i!=2)
            energy_int = 0;
        i++;
        t = 0;

        y[0] = 0;
        y[1] = start + i*dx;

        y[2] = start + i*dx;
        y[3] = start + i*dx;

        y[4] = 0;
        y[5] = 0;

        y[6] = start + i*dx;
        y[7] = 0;

        for(int j = N; j<2*N; j++)
            y[j] = 0;


        while(t<T_loop){
            verlet_step(t, dt[3], y, pendumlumsODE2D, gsl_dimension, NULL);
            //printf("kin energy Integral: %15e\n", energy_int);
            energy_int += fabs(pendulums_kin_energy2D(y));
            t += dt[3];
        }
        
        //kin_energy = pendulums_kin_energy2D(y);
        //printf("kin energy: %15e\n", kin_energy);
        printf("kin energy Integral: %15e\n", energy_int);
        printf("pos: %15e\n", start + i*dx);

        //printf("Das Equilibrium ist: %15e\n", i*dx);
    }while(energy_int > 1e-14);

    double equi = start + i*dx;
    printf("Das Equilibrium ist auf eine Genauigkeit von 10^-14: %15e\n", equi);
    return equi; //return Equilibrium
}

void y_of_x(double equi){
    /* ------- GSL Verwaltung -------------------------------------------------*/

    /* TODO */
    /* Dimensionalitaet des DGL-Systems */
    int gsl_dimension = 2*N;

    /* ------- Anfangswerte ---------------------------------------------------*/

    double y[gsl_dimension];
    double T_loop = T_max;

    /* TODO */
    /* Fuellen von Array y[] mit den Startwerten fuer jeden Punkt */
    y[0] = 0;
    y[1] = equi;

    y[2] = equi;
    y[3] = equi;

    y[4] = 0;
    y[5] = 0;

    y[6] = equi;
    y[7] = 0;

    for(int i = N; i<2*N; i++)
        y[i] = 0;
    
    //y[N] = 0.1;
    //y[N+1] = -0.1;
    /* ------- Simulations-Schleife -------------------------------------------*/

    /* Simulationszeit */
        double t = 0;

    /* Ausgabe-Dateien oeffnen */
    FILE *pos0_file = fopen("q0.dat", "w");
    FILE *pos1_file = fopen("q1.dat", "w");
    FILE *pos2_file = fopen("q2.dat", "w");
    FILE *pos3_file = fopen("q3.dat", "w");
    /* Schleife fuer RK4 */
    while (t < T_loop)
    {   
        //Einen Zeitschritt nach RK4 verfahren gehen
        verlet_step(t, dt[3], y, pendumlumsODE2D, gsl_dimension, NULL);

        /* TODO */

        fprintf(pos0_file, "%15e, %15e\n", y[0], y[1]);
        fprintf(pos1_file, "%15e, %15e\n", y[2], y[3]);
        fprintf(pos2_file, "%15e, %15e\n", y[4], y[5]);
        fprintf(pos3_file, "%15e, %15e\n", y[6], y[7]);

        //Zeitschritt gehen
        t += dt[3];
    }
    fclose(pos0_file);
    fclose(pos1_file);
    fclose(pos2_file);
    fclose(pos3_file);
     
    return;
}

/*************************    Program 7.2    ****************************/
/*                                                                      */
/************************************************************************/
/* Please Note:                                                         */
/*                                                                      */
/* (1) This function is written by Tao Pang in conjunction with         */
/*     his book, "An Introduction to Computational Physics," published  */
/*     by Cambridge University Press in 1997.                           */
/*                                                                      */
/* (2) No warranties, express or implied, are made for this program.    */
/*                                                                      */
/************************************************************************/

void mxwl (n,m,t,v)
/* This subroutine assigns velocities according to the Maxwell
   distribution. N is the total number of velocity components and
   M is the total number of degrees of freedom. T is the system
   temperature in the reduced units.  Copyright (c) Tao Pang 1997. */
int n,m;
double t;
double v[];
{
    int i;
    double v1,v2,ek,vs;
    void grnf();

    /* Assign a Gaussian distribution to each velocity component */

    for (i = 0; i < n-1; i = i+2)
    {
    grnf(&v1,&v2);
    v[i] = v1;
    v[i+1] = v2;
    }

    /* Scale the velocity to satisfy the partition theorem */

    ek = 0;
    for (i = 0; i < n; ++i)
    {
    ek = ek+v[i]*v[i];
    }
    vs = sqrt(ek/(m*t));
    for (i = 0; i < n; ++i)
    {
    v[i] = v[i]/vs;
    }
    }

    void grnf (x,y)
    /* Two Gaussian random numbers generated from two uniform
    random numbers.  Copyright (c) Tao Pang 1997. */
    double *x,*y;
    {
    double pi,r1,r2;
    double ranf();

    pi =  4*atan(1);
    r1 = -log(1-ranf());
    r2 =  2*pi*ranf();
    r1 =  sqrt(2*r1);
    *x  = r1*cos(r2);
    *y  = r1*sin(r2);
    }

    double ranf()
    /* Uniform random number generator x(n+1)= a*x(n) mod c
    with a = pow(7,5) and c = pow(2,31)-1.
    Copyright (c) Tao Pang 1997. */
    {
    const int ia=16807,ic=2147483647,iq=127773,ir=2836;
    int il,ih,it;
    double rc;
    extern int iseed;
    ih = iseed/iq;
    il = iseed%iq;
    it = ia*il-ir*ih;
    if (it > 0)
    {
    iseed = it;
    }
    else
    {
    iseed = ic+it;
    }
    rc = ic;
    return iseed/rc;
}

static double LambertW(double x)
    {
        // LambertW is not defined in this section
        if (x < -exp(-1))
            printf("The LambertW-function is not defined for %15e.", x);

        // computes the first branch for real values only

        // amount of iterations (empirically found)
        int amountOfIterations = fmax(4, (int)ceil(log10(x) / 3));

        // initial guess is based on 0 < ln(a) < 3
        double w = 3 * log(x + 1) / 4;

        // Halley's method via eqn (5.9) in Corless et al (1996)
        for (int i = 0; i < amountOfIterations; i++)
            w = w - (w * exp(w) - x) / (exp(w) * (w + 1) - (w + 2) * (w * exp(w) - x) / (2 * w + 2));

        return w;
    }


int main(void)
{
    /* ------- GSL Verwaltung -------------------------------------------------*/

    /* TODO */
    /* Berechnen Sie hier die korrekte Dimensionalitaet des DGL-Systems */
    int gsl_dimension = 2*N;

    /* Initialisieren des Systems fuer GSL
       Wir uebergeben Ihre erstellte Funktion und die Dimensionaliaet */
    //gsl_odeiv2_system SYSTEM = {pendumlumsODE, NULL, gsl_dimension, NULL};

    /* Auswahl des Integrators
       Wir waehlen hier einen Runge-Kutta 4. Ordnung (rk4). */
    //gsl_odeiv2_step *STEPPER =
    //    gsl_odeiv2_step_alloc(gsl_odeiv2_step_rk4, gsl_dimension);

    /* ------- Anfangswerte ---------------------------------------------------*/

    /* TODO */
    /* Erstellen Sie zwei Arrays mit der notwendigen Groesse. Sie muessen
       alle Positionen und Geschwindigkeiten fassen koennen. */
    double y[gsl_dimension];
    double yerr[gsl_dimension]; /* GSL benoetigt yerr */

    //double equi = find_equi(delta_x);
    //double equi = LambertW(pow(elem*elem/(M_PI*eps_0*r_0)*3.0/16.0,-r_0));
    double equi = 2;
    //printf("%15e\n", equi);
    double v[N];
    mxwl(N+1, 1, Temp, v);

    for(int j = 0; j<4; j++){

        /* TODO */
        /* Fuellen Sie das Array y[] mit den Startwerten */
        /*
        y[0] = 0;
        y[1] = equi;

        y[2] = equi;
        y[3] = equi;

        y[4] = 0;
        y[5] = 0;

        y[6] = equi;
        y[7] = 0;
        */

        y[0] = -equi;
        y[1] = 0;
        y[2] = equi;

        for(int i = 0; i<N; i++){
            //printf("v: %15e\n", v[i]);
            y[N+i] = v[i];
            //y[N+i] = 0;
        }

        double E_tot = pendulums_energy(y);

        /* ------- Simulations-Schleife -------------------------------------------*/

        /* Ausgabe-Dateien oeffnen */
        FILE *pos_file = fopen(pos_file_name, "w");
        FILE *energy_file = fopen(energy_file_name[j], "w");

        /* Simulationszeit */
        double t = 0;
 
        /* #####   HAUPTSCHLEIFE   ##### */
        while (t < T_max)
        {   
            /* step_apply befoertdert y[] zum naechsten Zeitpunkt */
            //gsl_odeiv2_step_apply(STEPPER, t, dt,
            //                      y, yerr, NULL, NULL, &SYSTEM);
            
            //euler_step(t, dt, y, pendumlumsODE, gsl_dimension, NULL);
            //rk4_step(t, dt[j], y, pendumlumsODE, gsl_dimension, NULL);
            verlet_step(t, dt[j], y, pendumlumsODE, gsl_dimension, NULL);
            //verlet_step(t, dt[j], y, pendumlumsODE2D, gsl_dimension, NULL);

            /* TODO */
            fprintf(energy_file, "%15e, %15e\n", t, fabs(E_tot-pendulums_energy(y)));
            //fprintf(energy_file, "%15e, %15e\n", t, pendulums_energy2D(y));
            fprintf(pos_file, "%15e", t);
            for(int i=0; i<N; i++){
                fprintf(pos_file, ", %15e", y[i]);
            }
            fprintf(pos_file, "\n");
            /* Geben Sie hier die Daten in die Dateien aus */

            t += dt[j];
        }
        fclose(pos_file);
        fclose(energy_file);    
    }

    //2D Daten fuer 4 Pendel
    //y_of_x(equi);
    
    return EXIT_SUCCESS;
}
