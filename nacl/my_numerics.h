#ifndef MYNUMERICS_H
#define MYNUMERICS_H

typedef int ode_func(double, const double[], double[], void*);
typedef void ode_func_v(double, const double[], double[], void*);

/* Numerical Integration of scalar 1D function */
double integrate(double left, double right, int N, double integrand(double));
double integrate_simpson(double left, double right, int N, double integrand(double));
//Simpson Integration for more Complex Functions with given step size
double integrate_simpson_2(double left, double right, double dx, double func(double, void*), void* params);

/* Implementations of the error function */
double erf_simpson(double x, double delta_x);
double erf_midpoint(double x, double delta_x);

/* Implementations of Numerical Differentiation */
double diff(double x, double delta, double func(double, void*), void *params);
double mid_diff(double x, double delta, double func(double, void*), void *params);

/* Implementation of root finder */
double find_root(double func(double, void*), double x0, double delta, const double rel_tol, const int max_iter);

/* Implementation of RK4 Method for Numerical calculations */
void rk4_step(double t, double delta_t, double y[], ode_func_v func, int dimension, void *params);
void rk4_step_int(double t, double delta_t, double y[], ode_func func, int dimension, void *params);
#endif
