import numpy as np
import matplotlib.pyplot as plt

data0 = np.loadtxt("q0.dat", delimiter = ',')
data1 = np.loadtxt("q1.dat", delimiter = ',')
data2 = np.loadtxt("q2.dat", delimiter = ',')
data3 = np.loadtxt("q3.dat", delimiter = ',')

axes = plt.gca()
#axes.set_yscale("log")
#axes.set_xscale("log")

axes.set(xlabel='x', ylabel='y')

axes.plot(data0[:,0], data0[:,1], color="black", label="Na_0")
axes.plot(data3[:,0], data3[:,1], color="black", label="Na_1", linestyle='dashed')
axes.plot(data1[:,0], data1[:,1], color="blue", label="Cl_0")
axes.plot(data2[:,0], data2[:,1], color="blue", label="Cl_1", linestyle='dashed')


axes.legend(loc="best")

plt.tight_layout()

plt.savefig("Na2Cl2 Cluster")
#plt.show()
