import numpy as np
import matplotlib.pyplot as plt

dt = [1e-2, 1e-3, 1e-4, 1e-5]
col = ["red", "blue", "black", "green"]
lines = ["solid", "dotted", "dashed", "-."]
#mark = ['o', '^', '<', '>']
#anteil = [10, 100, 1000, 10000]
v0 = 0.1
Energy = (v0*v0)/2

axes = plt.gca()
axes.set_yscale("log")
axes.set_xscale("log")

for i in range(4):
    #j = 0
    #markers_on = []
    #while j*(dt[i]*anteil[i]) < 20/(dt[i]*anteil[i]):
        #markers_on.extend([round(j)])
        #j += 1/(dt[i]*(anteil[i]/10.0))
    data = np.loadtxt("energy" + str(i) + ".dat", delimiter = ',')
    '''marker = mark[i], markevery=markers_on,'''
    axes.plot(data[:,0], data[:,1], label = "dt="+str(dt[i])+"s", color = col[i], linestyle = lines[i], alpha = 0.8)

#axes.set_title("Energiefehler zur verschiedenen $\Delta$t")
axes.set(xlabel='Zeit in $s$', ylabel='$\Delta$E in $eV$')
axes.legend()

plt.tight_layout()

plt.savefig("Energy vs Time")
