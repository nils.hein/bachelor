import numpy as np
import matplotlib.pyplot as plt

N = 3
t = np.arange(0,20,0.01)
v0 = 0.5

data = np.loadtxt("position.dat", delimiter = ',')

axes = plt.gca()
#axes.set_yscale("log")
#axes.set_xscale("log")

axes.set(xlabel='Zeit in $s$', ylabel='Position in $m$')

if N>1:
    for i in range(N):
        if i%2 == 0:
            if i==0:
                axes.plot(data[:,0], data[:,i+1], label="$Na^+$", color="black")
            else:
                axes.plot(data[:,0], data[:,i+1], color="black")
        else:
            axes.plot(data[:,0], data[:,i+1], label="$Cl^-$", linestyle="--", color="blue")
    axes.legend(bbox_to_anchor=(0.4, 1.17), loc=2, borderaxespad=0.)
else:
    axes.plot(data[:,0], data[:,1], label="", color="red", alpha = 0.8)

#axes.plot(t, v0*np.sin(t), label = "", color="blue", alpha = 0.3)

plt.tight_layout()

plt.savefig(str(N) + " Ions vs time")
plt.show()