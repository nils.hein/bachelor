#include <stdio.h>
#include <stdlib.h>
//#include <gsl/gsl_errno.h>
//#include <gsl/gsl_odeiv2.h>
#include "my_numerics.h"
#include <math.h>

/* TODO */
/*------------------------  PHYSIKALISCHE KONSTANTEN  -------------------------*/

const int N = 5;              // Anzahl der Pendel
const double k = 1;          // Federhaerte [N/m]
const double base_length = 1; // Basislaenge der Federn [m]
const double mass = 1;        // Masse der Pendel [kg]

/*-------------------------  SIMULATIONS-PARAMETER  ---------------------------*/

const double alpha_list[] = {0.1, 0.25, 0.5, 1};     //non-linearer Parameter
const double T_max = 20;
const double dt[] = {1e-2, 1e-3, 1e-4, 1e-5}; // Zeitliche Schrittweiten

static const char pos_file_name[] = "position.dat";
static const char energy_file_name[5][12] = {"energy0.dat", "energy1.dat", "energy2.dat", "energy3.dat"};

/*-------------------------  PHYSIKALISCHES SYSTEM  ---------------------------*/

/* TODO */
int pendumlumsODE(double t, const double y[], double f[], void *params)
{
    double alpha = *(double *)params;

    for(int i=0; i<2*N; i++){
        f[i]=0;
    }

    for(int i=0; i<N; i++){
        f[i] += y[N+i];
    }

    //double dx1 = y[2]-y[1]-base_length;
    //double dx2 = y[1]-y[0]-base_length;
    //double dx3 = 0;

    if(N>1){
        for(int i=1; i<N-1; i++){
            double dx1 = y[i+1]-(i+1);
            double dx2 = y[i]-i;
            double dx3 = y[i-1]-(i-1);
            //f[N+i] += ((y[i+1]-2.0*y[i]+y[i-1])+alpha*((y[i+1]-y[i])*(y[i+1]-y[i])-(y[i]-y[i-1])*(y[i]-y[i-1])))/mass;
            f[N+i] += (dx1-2*dx2+dx3+alpha*((dx1-dx2)*(dx1-dx2)-(dx2-dx3)*(dx2-dx3)))/mass;
        }
    }
    

    
    /*
    if(N>1){
        for(int i=1; i<N-1; i++){
            f[N+i] += (y[i+1]-2*y[i]+y[i-1])/mass;
        }
    }
    */
    /* -------------------------------------------------*/
    return 1;
}

/* TODO */
/* Bearbeiten Sie hier Aufgabe 12.9 */
double pendulums_energy(const double y[], void *params){

    double alpha = *(double *)params;
    double Energy = 0.0;
    double velocity = 0.0;
    double x = 0;
    for(int i = 0; i<N-1; i++){
        double dx = (y[i+1]-(i+1))-(y[i]-i);
        x += k*(dx*dx)/2+alpha*(dx*dx*dx)/3;
    }
    //x += k*(y[N-1]-y[N-2]-base_length)*(y[N-1]-y[N-2]-base_length)/2;

    for(int i=0; i<N; i++){
        velocity += y[N+i]*y[N+i];    
    }

    Energy = x + velocity*mass/2;
    

    /*
    int modes = N;
    double omega = 0.0;
    double Energy = 0.0;
    double x = 0.0;
    double dx = 0.0;

    double *Q = malloc(modes*sizeof(double));
    double *P = malloc(modes*sizeof(double));
    double *E_mode = malloc(modes*sizeof(double));

    for(int i = 0; i<modes; i++){
        Q[i] = 0;
        P[i] = 0;
        E_mode[i] = 0;
    }

    for(int j = 0; j<modes; j++){
        for(int i = 0; i<N-1; i++){
            dx = (y[i+1]-(i+1))-(y[i]-i);
            Q[j] += dx*sin(M_PI*(j+1)*(i+1)/N);
            P[j] += y[N+i]*sin(M_PI*(j+1)*(i+1)/N);
        }
        Q[j] *= sqrt(2.0/(N));
        //printf("Q: %15e\n", Q[j]);
        P[j] *= sqrt(2.0/(N));
        //printf("P: %15e\n", P[j]);
    }

    for(int i = 0; i<modes; i++){
        omega = 2.0*sin(M_PI*(i+1)/(2*N));
        //printf("%15e\n", omega);
        E_mode[i] = (P[i]*P[i]+omega*omega*Q[i]*Q[i])/2.0;
        //printf("%d-mode Energie: %15e\n", i+1, E_mode[i]);
        Energy += E_mode[i];
    }

    for(int i = 0; i<modes-1; i++){
        dx = (y[i+1]-(i+1))-(y[i]-i);
        x += alpha*(dx*dx*dx)/3;
    }

    Energy += x;
    
    free(Q);
    free(P);
    free(E_mode);
    */

    return Energy;
}

void euler_step(double t, double delta_t, double y[], ode_func func, int dimension, void *params){
    double *f = malloc(dimension*sizeof(double));
    func(t, y, f, params);
    for(int i = 0; i<dimension; i++){
        //f[i] = mid_diff(y[i], delta_t, func, NULL);
        y[i]+= f[i]*delta_t;
    }
    free(f);
    return;
}

/*
void rk4_step(double t, double delta_t, double y[], ode_func func, int dimension, void *params){
    double *f = malloc(dimension*sizeof(double));
    double *y_temp = malloc(dimension*sizeof(double));
    func(t, y, f, params);
    for(int i = 0; i<dimension; i++){
        double k1 = delta_t*f[i];
        for(int j = 0; j<dimension; j++)
            y_temp[j] = y[j] + k1/2;
        func(t+delta_t*1/2, y_temp, f, params);
        double k2 = delta_t*f[i];
        for(int j = 0; j<dimension; j++)
            y_temp[j] = y[j] + k2/2;
        func(t+delta_t*1/2, y_temp, f, params);
        double k3 = delta_t*f[i];
        for(int j = 0; j<dimension; j++)
            y_temp[j] = y[j] + k3;
        func(t+delta_t, y_temp, f, params);
        double k4 = delta_t*f[i];
        y[i]+=(k1+2*k2+2*k3+k4)/6;
    }
    free(f);
    free(y_temp);
    return;
}
*/  


void verlet_step(double t, double delta_t, double y[], ode_func func, int dimension, void* params){
    double *a1 = malloc(dimension*sizeof(double));
    double *a2 = malloc(dimension*sizeof(double));

    func(t, y, a1, params);
    for(int i=0; i<dimension/2; i++){
        y[i] = y[i] + a1[i]*delta_t + a1[N+i]*delta_t*delta_t/2;
    }
    func(t+delta_t, y, a2, params);
    for(int i=0; i<dimension/2; i++){
        y[N+i] = y[N+i] + delta_t/2*(a2[N+i]+a1[N+i]);
    }

    free(a1);
    free(a2);
}
/*-----------------------------------------------------------------------------*/

int main(void)
{
    /* ------- GSL Verwaltung -------------------------------------------------*/

    /* TODO */
    /* Berechnen Sie hier die korrekte Dimensionalitaet des DGL-Systems */
    int gsl_dimension = 2*N;

    /* Initialisieren des Systems fuer GSL
       Wir uebergeben Ihre erstellte Funktion und die Dimensionaliaet */
    //gsl_odeiv2_system SYSTEM = {pendumlumsODE, NULL, gsl_dimension, NULL};

    /* Auswahl des Integrators
       Wir waehlen hier einen Runge-Kutta 4. Ordnung (rk4). */
    //gsl_odeiv2_step *STEPPER =
    //    gsl_odeiv2_step_alloc(gsl_odeiv2_step_rk4, gsl_dimension);

    /* ------- Anfangswerte ---------------------------------------------------*/

    /* TODO */
    /* Erstellen Sie zwei Arrays mit der notwendigen Groesse. Sie muessen
       alle Positionen und Geschwindigkeiten fassen koennen. */
    double y[gsl_dimension];
    double yerr[gsl_dimension]; /* GSL benoetigt yerr */

    double v_0[] = {0.1, 0.25, 0.4, 0.5};
    double E_0 = 0.0;

    for(int j = 0; j<4; j++){

        E_0 = (v_0[3]*v_0[3]/2.0);
        /* TODO */
        /* Fuellen Sie das Array y[] mit den Startwerten */
        for(int i = 0; i<N; i++)
            y[i] = i*base_length;
        //y[1] = sqrt(2);
        y[N] = 0;
        //y[N+1] = 1.0/(50*sqrt(5));
        y[N+1] = v_0[3];
        for(int i = N+2; i<2*N; i++)
            y[i] = 0;

        /* ------- Simulations-Schleife -------------------------------------------*/

        /* Ausgabe-Dateien oeffnen */
        FILE *pos_file = fopen(pos_file_name, "w");
        FILE *energy_file = fopen(energy_file_name[j], "w");

        /* Simulationszeit */
        double t = 0;

        /* #####   HAUPTSCHLEIFE   ##### */
        while (t < T_max)
        {   
            /* step_apply befoertdert y[] zum naechsten Zeitpunkt */
            //gsl_odeiv2_step_apply(STEPPER, t, dt,
            //                      y, yerr, NULL, NULL, &SYSTEM);
            
            //euler_step(t, dt[j], y, pendumlumsODE, gsl_dimension, NULL);
            //rk4_step(t, dt[j], y, pendumlumsODE, gsl_dimension, NULL);
            verlet_step(t, dt[0], y, pendumlumsODE, gsl_dimension, &alpha_list[j]);

            /* TODO */
            fprintf(energy_file, "%15e, %15e\n", t, fabs(E_0-pendulums_energy(y, &alpha_list[j])));
            fprintf(pos_file, "%15e", t);
            for(int i=0; i<N; i++){
                fprintf(pos_file, ", %15e", y[i]);
            }
            fprintf(pos_file, "\n");
            /* Geben Sie hier die Daten in die Dateien aus */

            t += dt[0];
        }
        fclose(pos_file);
        fclose(energy_file);    
    }
    
    return EXIT_SUCCESS;
}
