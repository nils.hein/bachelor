import numpy as np
import matplotlib.pyplot as plt
import glob
from PIL import Image

N = 32
t = np.arange(0,20,0.01)
v0 = 0.5


def make_gif(frame_folder):
    frames = [Image.open(image) for image in glob.glob(f"{frame_folder}/*.png")]
    frame_one = frames[0]
    frame_one.save("fpu.gif", format="GIF", append_images=frames,
               save_all=True, duration=10, loop=2)

data = np.loadtxt("position.dat", delimiter = ',')

axes = plt.gca()
#axes.set_yscale("log")
#axes.set_xscale("log")

for i in range(len(data[:,0])):
    axes.set(xlabel='Ruhelage', ylabel='Auslenkung')
    axes.plot(0, 0.4, color="black", alpha = 0.001)
    axes.plot(0, -0.4, color="black", alpha = 0.001)
    for j in range(N):
        axes.scatter(j, data[i,j+1]-j, color="black")
    plt.tight_layout()
    plt.savefig("frames/Zeitschritt " + str(i))
    plt.cla()

make_gif("frames/")