import numpy as np
import matplotlib.pyplot as plt

dt = [1e-2, 1e-3, 1e-4, 1e-5]
col = ["red", "blue", "black", "green"]
lines = ["solid", "dotted", "dashed", "-."]
#mark = ['o', '^', '<', '>']
#anteil = [10, 100, 1000, 10000]
v0 = 0.1
Energy = (v0*v0)/2

axes = plt.gca()
axes.set_yscale("log")
axes.set_xscale("log")

for i in range(4):
    #j = 0
    #markers_on = []
    #while j*(dt[i]*anteil[i]) < 20/(dt[i]*anteil[i]):
        #markers_on.extend([round(j)])
        #j += 1/(dt[i]*(anteil[i]/10.0))
    data = np.loadtxt("energy" + str(i) + ".dat", delimiter = ',')
    '''marker = mark[i], markevery=markers_on,'''
    axes.plot(data[:,0], data[:,1], label = "dt="+str(dt[i])+"s", color = col[i], linestyle = lines[i], alpha = 0.8)

#axes.set_title("Energiefehler zur verschiedenen $\Delta$t")
axes.set(xlabel='Zeit in $s$', ylabel='$\Delta$E in $m\omega^2b^2$')
axes.legend()

"""
fig, axes = plt.subplots(2,2)
#axes.set_yscale("log")
#axes.set_xscale("log")

#for i in range(4):
#    data = np.loadtxt("energy" + str(i) + ".dat", delimiter = ',')
#    
data = np.loadtxt("energy" + str(0) + ".dat", delimiter = ',')
axes[0,0].plot(data[:,0], data[:,1])
axes[0,0].set_title('dt = ' + str(dt[0]))

data = np.loadtxt("energy" + str(1) + ".dat", delimiter = ',')
axes[0,1].plot(data[:,0], data[:,1])
axes[0,1].set_title('dt = ' + str(dt[1]))

data = np.loadtxt("energy" + str(2) + ".dat", delimiter = ',')
axes[1,0].plot(data[:,0], data[:,1])
axes[1,0].set_title('dt = ' + str(dt[2]))

data = np.loadtxt("energy" + str(3) + ".dat", delimiter = ',')
axes[1,1].plot(data[:,0], data[:,1])
axes[1,1].set_title('dt = ' + str(dt[3]))

for ax in axes.flat:
    ax.set(xlabel='Zeit in $s$', ylabel='Energy in $m\omega^2b^2$')
"""
    
plt.tight_layout()

plt.savefig("Energy vs Time")
#plt.show()

