import numpy as np
import matplotlib.pyplot as plt

N = 1
t = np.arange(0,20,0.01)
v0 = 0.5
dt = [1e-2, 1e-3, 1e-4, 1e-5]

data = np.loadtxt("position3.dat", delimiter = ',')

axes = plt.gca()
#axes.set_yscale("log")
#axes.set_xscale("log")

axes.set(xlabel='Zeit in $s$', ylabel='Position in $m$')

if N>1:
    for i in range(N):
        axes.plot(data[:,0], data[:,i+1], label="", color="black")
    axes.legend(bbox_to_anchor=(0.25, 1.15), loc=2, borderaxespad=0.)
else:
    
    fig, axes = plt.subplots(2)
    data = np.loadtxt("position" + str(0) + ".dat", delimiter = ',')
    t = np.arange(dt[0],20+dt[0],dt[0])
    #print(t)
    #print((v0)*np.sin(t))
    #axes[0].plot(t, v0*np.sin(t), label = "analytische Lösung", color="red", linestyle = "solid", alpha = 0.8)
    axes[0].plot(data[:,0], (data[:,1])-(v0)*np.sin(t), label="analytische und numerische Positionsdifferenz", color="blue", linestyle = "solid", alpha = 0.8)
    axes[0].set_title('dt = ' + str(dt[0]) + 's')
    axes[0].legend(bbox_to_anchor=(0.15, 1.75), loc=2, borderaxespad=0.)

    data = np.loadtxt("position" + str(2) + ".dat", delimiter = ',')
    t = np.arange(dt[2],20+2*dt[2],dt[2])
    #print(t)
    #axes[1].plot(t, v0*np.sin(t), label = "analytische Lösung", color="red", linestyle = "solid", alpha = 0.8)
    axes[1].plot(data[:,0], data[:,1]-(v0)*np.sin(t), label="numerische Lösung nach Verlet", color="blue", linestyle = "solid", linewidth = 0.05, alpha = 0.8)
    axes[1].set_title('dt = ' + str(dt[2]) + 's')

    for ax in axes.flat:
        ax.set(xlabel='Zeit in $s$', ylabel='Position in $m$')

    
    """
    i = 0
    markers_on = []
    while i*(dt[2]*100) < 20/(dt[2]*100):
        markers_on.extend([round(i)])
        i += 1/(dt[2]*10)
    t = np.arange(dt[2],20+dt[2],dt[2])
    data = np.loadtxt("position" + str(2) + ".dat", delimiter = ',')
    axes.plot(t, v0*np.sin(t), label = "analytische Lösung", color="red", linestyle = "solid", marker = 'o', markevery=markers_on, alpha = 0.8)
    axes.plot(data[:,0], data[:,1], label="numerische Lösung nach Verlet", color="blue", linestyle = "dashed", marker ='^', markevery=markers_on, alpha = 0.8)
    axes.set_title('dt = ' + str(dt[2]) +'s')
    axes.set(xlabel='Zeit in $s$', ylabel='Position in $m$')
    axes.legend(bbox_to_anchor=(0.25, 1.25), loc=2, borderaxespad=0.)
    """

   

plt.tight_layout()

plt.savefig(str(N) + " Pendulums Position vs Time", bbox_inches='tight')
#plt.show()
