/*---------------------------------------------*\
|  CWR 2023                                     |
|  Blatt 4 - Aufgabe 12                         |
|  Original Author: mail@saschalambert.de       |
\*---------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
//#include <gsl/gsl_errno.h>
//#include <gsl/gsl_odeiv2.h>
#include "my_numerics.h"
#include <tgmath.h>

/* TODO */
/*------------------------  PHYSIKALISCHE KONSTANTEN  -------------------------*/

const int N = 100;              // Anzahl der Pendel
const double k = 1;          // Federhaerte [N/m]
const double base_length = 1; // Basislaenge der Federn [m]
const double mass = 1;        // Masse der Pendel [kg]

/*-------------------------  SIMULATIONS-PARAMETER  ---------------------------*/

const double T_max = 100.0;
const double dt[] = {1e-2, 1e-3, 1e-4, 1e-5}; // Zeitliche Schrittweiten

static const char pos_file_name[5][14] = {"position0.dat", "position1.dat", "position2.dat", "position3.dat"};
static const char energy_file_name[5][12] = {"energy0.dat", "energy1.dat", "energy2.dat", "energy3.dat"};

/*-------------------------  PHYSIKALISCHES SYSTEM  ---------------------------*/

/* TODO */
int pendumlumsODE(double t, const double y[], double f[], void *params)
{
    /* Loesung fuer Aufgabe 12.4 kommt hier dazwischen: */
    for(int i=0; i<2*N; i++){
        f[i]=0;
    }

    for(int i=0; i<N; i++){
        f[i] += y[N+i];
    }

    f[N] += -k*y[0]/mass;

    if(N>1){
        for (int i = 0; i < N - 1; ++i)
        {
            double delta_x = y[i + 1] - y[i];
            double spring_force = k * (delta_x - base_length);
            f[N + i] += spring_force / mass;
            f[N + i + 1] -= spring_force / mass;
        }
    }
    /* -------------------------------------------------*/
    return 1;
}

/* TODO */
/* Bearbeiten Sie hier Aufgabe 12.9 */
double pendulums_energy(const double y[]){
    //double Energy = 0.0;
    double verlocity = 0.0;
    double x = y[0]*y[0];
    for(int i = 0; i<N-1; i++){
        double dx = y[i+1]-y[i]-base_length;
        x += dx*dx;
        verlocity += y[N+i]*y[N+i];
    }
    verlocity += y[2*N-1]*y[2*N-1];
    //printf("v: %15e\n", verlocity);
    //printf("x: %15e\n", x);
    double Energy = (k*x+mass*verlocity)/2;
    //printf("E: %15e\n", Energy);
    return Energy;
}

void euler_step(double t, double delta_t, double y[], ode_func func, int dimension, void *params){
    double *f = malloc(dimension*sizeof(double));
    func(t, y, f, params);
    for(int i = 0; i<dimension; i++){
        //f[i] = mid_diff(y[i], delta_t, func, NULL);
        y[i]+= f[i]*delta_t;
    }
    free(f);
    return;
}

/*
void rk4_step(double t, double delta_t, double y[], ode_func func, int dimension, void *params){
    double *f = malloc(dimension*sizeof(double));
    double *y_temp = malloc(dimension*sizeof(double));
    func(t, y, f, params);
    for(int i = 0; i<dimension; i++){
        double k1 = delta_t*f[i];
        for(int j = 0; j<dimension; j++)
            y_temp[j] = y[j] + k1/2;
        func(t+delta_t*1/2, y_temp, f, params);
        double k2 = delta_t*f[i];
        for(int j = 0; j<dimension; j++)
            y_temp[j] = y[j] + k2/2;
        func(t+delta_t*1/2, y_temp, f, params);
        double k3 = delta_t*f[i];
        for(int j = 0; j<dimension; j++)
            y_temp[j] = y[j] + k3;
        func(t+delta_t, y_temp, f, params);
        double k4 = delta_t*f[i];
        y[i]+=(k1+2*k2+2*k3+k4)/6;
    }
    free(f);
    free(y_temp);
    return;
}
*/  


void verlet_step(double t, double delta_t, double y[], ode_func func, int dimension, void* params){
    double *a1 = malloc(dimension*sizeof(double));
    double *a2 = malloc(dimension*sizeof(double));

    func(t, y, a1, params);
    for(int i=0; i<dimension/2; i++){
        y[i] = y[i] + a1[i]*delta_t + a1[N+i]*delta_t*delta_t/2;
    }
    func(t+delta_t, y, a2, params);
    for(int i=0; i<dimension/2; i++){
        y[N+i] = y[N+i] + delta_t/2*(a2[N+i]+a1[N+i]);
    }

    free(a1);
    free(a2);
}
/*-----------------------------------------------------------------------------*/

int main(void)
{
    /* ------- GSL Verwaltung -------------------------------------------------*/

    /* TODO */
    /* Berechnen Sie hier die korrekte Dimensionalitaet des DGL-Systems */
    int gsl_dimension = 2*N;

    /* Initialisieren des Systems fuer GSL
       Wir uebergeben Ihre erstellte Funktion und die Dimensionaliaet */
    //gsl_odeiv2_system SYSTEM = {pendumlumsODE, NULL, gsl_dimension, NULL};

    /* Auswahl des Integrators
       Wir waehlen hier einen Runge-Kutta 4. Ordnung (rk4). */
    //gsl_odeiv2_step *STEPPER =
    //    gsl_odeiv2_step_alloc(gsl_odeiv2_step_rk4, gsl_dimension);

    /* ------- Anfangswerte ---------------------------------------------------*/

    /* TODO */
    /* Erstellen Sie zwei Arrays mit der notwendigen Groesse. Sie muessen
       alle Positionen und Geschwindigkeiten fassen koennen. */
    double y[gsl_dimension];
    double yerr[gsl_dimension]; /* GSL benoetigt yerr */

    double v_0[] = {0.1, 0.25, 0.4, 0.5};
    double E_0 = 0.0;

    for(int j = 0; j<4; j++){

        E_0 = (v_0[3]*v_0[3]/2.0);

        /* TODO */
        /* Fuellen Sie das Array y[] mit den Startwerten */
        for(int i = 0; i<N; i++)
            y[i] = i*base_length;
        y[N] = v_0[3];
        for(int i = N+1; i<2*N; i++)
            y[i] = 0;
        /* ------- Simulations-Schleife -------------------------------------------*/

        /* Ausgabe-Dateien oeffnen */
        FILE *pos_file = fopen(pos_file_name[j], "w");
        FILE *energy_file = fopen(energy_file_name[j], "w");

        /* Simulationszeit */
        double t = 0;

        /* #####   HAUPTSCHLEIFE   ##### */
        while (t < T_max)
        {   
            /* step_apply befoertdert y[] zum naechsten Zeitpunkt */
            //gsl_odeiv2_step_apply(STEPPER, t, dt,
            //                      y, yerr, NULL, NULL, &SYSTEM);
            
            //euler_step(t, dt, y, pendumlumsODE, gsl_dimension, NULL);
            //rk4_step(t, dt, y, pendumlumsODE, gsl_dimension, NULL);
            verlet_step(t, dt[j], y, pendumlumsODE, gsl_dimension, NULL);

            /* TODO */
            fprintf(energy_file, "%15e, %15e\n", t, fabs(E_0-pendulums_energy(y)));
            fprintf(pos_file, "%15e", t);
            for(int i=0; i<N; i++){
                fprintf(pos_file, ", %15e", y[i]);
            }
            fprintf(pos_file, "\n");
            /* Geben Sie hier die Daten in die Dateien aus */

            t += dt[j];
        }
        fclose(pos_file);
        fclose(energy_file);    
    }
    
    return EXIT_SUCCESS;
}
