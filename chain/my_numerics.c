#include<stdio.h>
#include<tgmath.h>
#include<stdlib.h>

typedef int ode_func(double, const double[], double[], void*);
typedef void ode_func_v(double, const double[], double[], void*);

static double mn_erf_func(double x){
	return exp(-(x*x))*2/(sqrt(M_PI));
}

double integrate(double left, double right, int N, double integrand(double)){
	double sum = 0.0;
	double delta_y = (right-left)/N;

	for(int k = 0; k < N; k++){
                double y = left + (k + 0.5) * delta_y;
                double f = integrand(y);
                double A = f * delta_y;
                sum += A;
        }
	return sum;
}

double integrate_simpson(double left, double right, int N, double integrand(double)){
	double sum = 0.0;
	double delta_x = (right-left)/N;
	for(int i = 0; i<N; i++){
		double x = left + i*delta_x;
		double f_mid = integrand(x + delta_x*0.5);
		sum += integrand(x) + 4*f_mid + integrand(x+delta_x);
	}
	sum *= delta_x/6;
	return sum;
}

double integrate_simpson_2(double left, double right, double dx, double func(double, void*), void* params){
	//initialize sum and k from parameters
	double sum = 0.0;
	//Calculation for each step dx until right boundary reached
	for(int i = 0; left+(i*dx) < right; i++){
		//Left side of step
		double x = left + i*dx;
		//function value in the middle of the current step
		double f_mid = func(x + dx*0.5, params);
		//f(x)+4*f_mid+f(x+dx) Calculation of Simpson Area for the Current step
		sum += func(x, params) + f_mid*4.0 + func(x+dx, params);
	}
	//Normalizing the Area for Step Size
	sum *= dx/6;
	//Returning Numeric Value of Integral
	return sum;
} 

double erf_midpoint(double x, double delta_x){
        int N = fabs(x/delta_x);
        double sum = integrate(0, x, N, mn_erf_func);
        sum *= 2/(sqrt(M_PI));
        return sum;
}

double erf_simpson(double x, double delta_x){
        int N = fabs(x/delta_x);
        double sum = integrate_simpson(0, x, N, mn_erf_func);
        sum *= 2/(sqrt(M_PI));
        return sum;
}

double diff(double x, double delta, double func(double, void *), void *params){
	return (func(x+delta, params)-func(x, params))/delta;
}

double mid_diff(double x, double delta, double func(double, void *), void *params) {
	return (func(x+delta, params)-func(x-delta, params))/(2.0*delta);
}

double find_root(double func(double, void*), double x0, double delta, const double rel_tol, const int max_iter){
	double x = x0;
	int iteration = 0;
	while(iteration < max_iter){
		double f = func(x, NULL);
		double df = diff(x, delta, func, NULL);
		double x_old = x;

		x -= f/df;

		if(fabs(x_old-x)/fabs(x) < rel_tol)
			break;
		iteration++;
	}
	return x;
}

//Numerical Method for Calculations
void rk4_step(double t, double delta_t, double y[], ode_func_v func, int dimension, void *params){
    //reserviert Speicher f und y_temp um sequentielle Berechnungen auszufuehren
    double *k1 = malloc(dimension*sizeof(double));
    double *k2 = malloc(dimension*sizeof(double));
    double *k3 = malloc(dimension*sizeof(double));
    double *k4 = malloc(dimension*sizeof(double));
    double *y_temp = malloc(dimension*sizeof(double));
    //Werte fuer den aktuellen Schritt der ODE werden berechnet
    func(t, y, k1, params);
    //Berechnen des RK2 verfahren mit temporaeren y_temp werten um Ergebniss nicht zu verfaelschen (fuer mathematische Einzelheiten siehe Skript oder Uebung 13)
    for(int j = 0; j<dimension; j++){
        k1[j] *= delta_t;
        y_temp[j] = y[j] + k1[j]/2.0;
    }

    func(t+delta_t/2.0, y_temp, k2, params);

    for(int j = 0; j<dimension; j++){
        k2[j] *= delta_t;
        y_temp[j] = y[j] + k2[j]/2.0;
    }

    func(t+delta_t/2.0, y_temp, k3, params);

    for(int j = 0; j<dimension; j++){
        k3[j] *= delta_t;
        y_temp[j] = y[j] + k3[j];
    }

    func(t+delta_t, y_temp, k4, params);

    for(int j = 0; j<dimension; j++){
        k4[j] *= delta_t;
        y[j] += (k1[j]+2.0*k2[j]+2.0*k3[j]+k4[j])/6.0;
    }

    //Speicherfreigabe
    free(y_temp);
    free(k1);
    free(k2);
    free(k3);
    free(k4);
    return;
}

void rk4_step_int(double t, double delta_t, double y[], ode_func func, int dimension, void *params){
    //reserviert Speicher f und y_temp um sequentielle Berechnungen auszufuehren
    double *k1 = malloc(dimension*sizeof(double));
    double *k2 = malloc(dimension*sizeof(double));
    double *k3 = malloc(dimension*sizeof(double));
    double *k4 = malloc(dimension*sizeof(double));
    double *y_temp = malloc(dimension*sizeof(double));
    //Werte fuer den aktuellen Schritt der ODE werden berechnet
    func(t, y, k1, params);
    //Berechnen des RK2 verfahren mit temporaeren y_temp werten um Ergebniss nicht zu verfaelschen (fuer mathematische Einzelheiten siehe Skript oder Uebung 13)
    for(int j = 0; j<dimension; j++){
        k1[j] *= delta_t;
        y_temp[j] = y[j] + k1[j]/2.0;
    }

    func(t+delta_t/2.0, y_temp, k2, params);

    for(int j = 0; j<dimension; j++){
        k2[j] *= delta_t;
        y_temp[j] = y[j] + k2[j]/2.0;
    }

    func(t+delta_t/2.0, y_temp, k3, params);

    for(int j = 0; j<dimension; j++){
        k3[j] *= delta_t;
        y_temp[j] = y[j] + k3[j];
    }

    func(t+delta_t, y_temp, k4, params);

    for(int j = 0; j<dimension; j++){
        k4[j] *= delta_t;
        y[j] += (k1[j]+2.0*k2[j]+2.0*k3[j]+k4[j])/6.0;
    }

    //Speicherfreigabe
    free(y_temp);
    free(k1);
    free(k2);
    free(k3);
    free(k4);
    return;
}
